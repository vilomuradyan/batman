package com.vilo.batman.di

import com.vilo.detail.di.featureDetailModule
import com.vilo.home.di.featureHomeModule
import com.vilo.local.di.localModule
import com.vilo.remote.di.createRemoteModule
import com.vilo.repository.di.repositoryModule

val appComponent= listOf(
    createRemoteModule("http://www.omdbapi.com"),
    repositoryModule,
    featureHomeModule, featureDetailModule, localModule
)