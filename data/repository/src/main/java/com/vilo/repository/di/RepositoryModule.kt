package com.vilo.repository.di

import com.vilo.repository.AppDispatchers
import com.vilo.repository.BatmanRepository
import com.vilo.repository.BatmanRepositoryImpl
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module.module

val repositoryModule = module {
    factory { AppDispatchers(Dispatchers.Main, Dispatchers.IO) }
    factory { BatmanRepositoryImpl(
        get(),
        get()
    ) as BatmanRepository
    }
}