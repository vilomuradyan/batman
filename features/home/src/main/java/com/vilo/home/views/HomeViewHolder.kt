package com.vilo.home.views

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vilo.home.HomeViewModel
import com.vilo.home.databinding.ItemHomeBinding
import com.vilo.model.Search

class HomeViewHolder(parent: View): RecyclerView.ViewHolder(parent) {

    private val binding = ItemHomeBinding.bind(parent)

    fun bindTo(search: Search, viewModel: HomeViewModel) {
        binding.search = search
        binding.viewmodel = viewModel
    }
}