package com.vilo.detail.di

import com.vilo.detail.DetailImageViewModel
import com.vilo.detail.DetailViewModel
import com.vilo.detail.domain.GetUserDetailUseCase
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val featureDetailModule = module {
    factory { GetUserDetailUseCase(get()) }
    viewModel { DetailViewModel(get(), get()) }
    viewModel { DetailImageViewModel() }
}